# base image
FROM node:10.15.3

# set working directory
WORKDIR /app

# add `/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH

# install and cache app dependencies
# add app
COPY . /app
RUN npm install
RUN npm run build

EXPOSE 3000
# start app
CMD [ "npm" ,"run" ,"start" ]